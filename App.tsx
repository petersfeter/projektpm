import { StatusBar } from 'expo-status-bar';
import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "./components/views/homeScreen";
import posterunekScreen from "./components/views/posterunekScreen";
import molochScreen from "./components/views/molochScreen";
import borgoScreen from "./components/views/borgoScreen";
import hegemoniaScreen from "./components/views/hegemoniaScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer >
      <Stack.Navigator screenOptions={{
        headerStyle: {
          fontFamily: "Roboto",
          color: "white"
        }
      }}>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Wybierz nację',
            headerStyle: {
              backgroundColor: "black",
              color: "white",
            },
            headerTintColor: "black",
            headerTitleStyle: {
              fontFamily: "Roboto",
              color: "white"
            }
          }}
        />
        <Stack.Screen
          name="Posterunek"
          component={posterunekScreen}
          options={{
            title: 'Posterunek',
            headerStyle: {
              backgroundColor: "black",
              fontFamily: "sans-serif-light",
              color: "white"
            },
            headerTintColor: "black",
            headerTitleStyle: {
              fontFamily: "Roboto",
              color: "white"
            }
          }}
        />
        <Stack.Screen
          name="Moloch"
          component={molochScreen}
          options={{
            title: 'Moloch',
            headerStyle: {
              backgroundColor: "black",
              fontFamily: "sans-serif-light",
              color: "white"
            },
            headerTintColor: "black",
            headerTitleStyle: {
              fontFamily: "Roboto",
              color: "white"
            }
          }}
        />
        <Stack.Screen
          name="Borgo"
          component={borgoScreen}
          options={{
            title: 'Borgo',
            headerStyle: {
              backgroundColor: "black",
              fontFamily: "sans-serif-light",
              color: "white"
            },
            headerTintColor: "black",
            headerTitleStyle: {
              fontFamily: "Roboto",
              color: "white"
            }
          }}
        />
        <Stack.Screen
          name="Hegemonia"
          component={hegemoniaScreen}
          options={{
            title: 'Hegemonia',
            headerStyle: {
              backgroundColor: "black",
              fontFamily: "sans-serif-light",
              color: "white"
            },
            headerTintColor: "black",
            headerTitleStyle: {
              fontFamily: "Roboto",
              color: "white"
            }
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}