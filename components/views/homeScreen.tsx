import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground } from 'react-native';
import 'react-native-gesture-handler';

export default class HomeScreen extends React.Component {

    screen = (name) => {
        this.props.navigation.navigate(name);
    }

    render() {
        return (
            <View style={styles.container}>

                <View style={styles.row}>
                    <TouchableOpacity onPress={() => this.screen('Posterunek')}>
                        <ImageBackground source={{
                            uri: "https://neuroshimahex.pl/gfx/posterunek/posterunek-sztab.png",
                            cache: 'only-if-cached'
                        }}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Posterunek</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

                <View style={styles.row}>
                    <TouchableOpacity onPress={() => this.screen('Moloch')}>
                        <ImageBackground source={{
                            uri: "https://neuroshimahex.pl/gfx/moloch/moloch-sztab.png",
                            cache: 'only-if-cached'
                        }}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Moloch</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

                <View style={styles.row}>
                <TouchableOpacity onPress={() => this.screen('Borgo')}>
                        <ImageBackground source={{
                            uri: "https://neuroshimahex.pl/gfx/borgo/borgo-sztab.png",
                            cache: 'only-if-cached'
                        }}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Borgo</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

                <View style={styles.row}>
                <TouchableOpacity onPress={() => this.screen('Hegemonia')}>
                        <ImageBackground source={{
                            uri: "https://neuroshimahex.pl/gfx/hegemonia/hegemonia-sztab.png",
                            cache: 'only-if-cached'
                        }}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Hegemonia</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100%",
        overflow: "hidden"
    },
    row: {
        display: "flex",
        width: "100%",
        height: "25%",
        borderBottomWidth: 0,
        borderTopWidth: 0,
        borderColor: "black"
    },
    buttonText: {
        color: "white",
        textAlign: "center",
        fontSize: 50,
        textShadowColor: "black",
        textShadowRadius: 30
    },
    button: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%"
    }
});