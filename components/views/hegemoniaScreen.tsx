import React, { Props } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ImageBackground } from 'react-native';
import json from '../../assets/data/hegemonia.js';

export default class HegemoniaScreen extends React.Component {
    state: {
        counter: number;
        tmpJson: any;
        rand: any;
        drawnArray: any;
    }

    constructor(props: any) {
        super(props);
        this.state = {
            counter: 0,
            rand: undefined,
            tmpJson: json,
            drawnArray: []
        }
    }
    helper = [];
    draw() {
        if (this.state.counter !== 3) {
            this.setState({ rand: 1 });
            const tmp = this.state.tmpJson[Math.floor(Math.random() * this.state.tmpJson.length)];
            //console.log(tmp);
            if (tmp) {
                this.setState({ rand: tmp });
            }
            this.helper.push(tmp);
            this.setState({ drawnArray: this.helper });
            let tmpJson2 = this.state.tmpJson.filter(item => item.id != tmp.id);
            this.setState({ tmpJson: tmpJson2 });

            this.setState((prevState) => ({ counter: prevState.counter + 1 }));

            //console.log(this.state);
        }
    };
    play(id) {
        let tmpState = this.state;
        console.log(id);
        let tmpArray = tmpState.drawnArray.filter(item => item.id != id);
        this.helper = tmpArray;
        this.setState({ drawnArray: tmpArray });
        this.setState((prevState) => ({ counter: prevState.counter - 1 }));

    };
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <TouchableOpacity style={this.state.counter === 3 ? styles.buttonDisabled : styles.buttonDraw} onPress={() => this.draw()}>
                        <Text style={styles.buttonText}>Wylosuj kartę</Text>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 34, backgroundColor: "black", color: "white",fontFamily:"Roboto" }}>{this.state.tmpJson.length}/34</Text>
                </View>
                <View >
                    {
                        this.state.drawnArray.map(item => (
                            <View style={styles.rowTile}>
                                <View>
                                    <TouchableOpacity style={styles.button} onPress={() => this.play(item.id)}>
                                        <Text style={styles.buttonText}>Zagraj</Text>
                                    </TouchableOpacity>
                                </View>
                                <ImageBackground source={{
                                    uri: item.imgUrl,
                                    cache: 'only-if-cached'
                                }}
                                    style={styles.img}>
                                </ImageBackground>
                            </View>))
                    }
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor:"black",
        display: "flex",
        flexDirection: "column",
        width: "100%",
        height: "100%",
        overflow: "hidden"
    },
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent:"space-between",
        alignItems:"center",
        width: "100%",
        height: 100,
        borderBottomWidth: 0,
        borderTopWidth: 0,
        borderColor: "black"
    },
    buttonText: {
        color: "white",
        textAlign: "center",
        fontSize: 30,
        textShadowColor: "black",
        textShadowRadius: 30,
        fontFamily:"Roboto"
    },
    button: {
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        width: "100%"
    },
    img: {
        backgroundColor:"black",
        width:200,
        height:200
    },
    rowTile: {
        display: "flex",
        flexDirection: "row",
        width: "100%",
        minHeight:200,
        height: "25%",
        justifyContent:"space-around",
        alignItems:"center",
        borderBottomWidth: 0,
        borderTopWidth: 0
    },
    buttonDraw: {
    },
    buttonDisabled: {
        opacity: 0.7
    }
});